#include <gfxfont.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SPITFT.h>
#include <Adafruit_SPITFT_Macros.h>
#include <Adafruit_SSD1306.h>
#include <splash.h>

// Pin definitions
#define R_BUTTON              4
#define W_BUTTON              2
#define DO_NOT_CLICK_LED      6
#define CLICK_LED             7
#define R_SCORE_0             8
#define R_SCORE_1             9
#define R_SCORE_2             10
#define W_SCORE_0             11
#define W_SCORE_1             12
#define W_SCORE_2             13

// Display
#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 64 // OLED display height, in pixels

// Declaration for an SSD1306 display connected to I2C (SDA, SCL pins)
#define OLED_RESET     4 // Reset pin # (or -1 if sharing Arduino reset pin)

Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);
int pwhite_score;
int pred_score;
int i, j, k;
long randNbr0, randNbr1;

void showtext(const char* text) {
  display.clearDisplay();
  display.setTextSize(2); // Draw 2X-scale text
  display.setTextColor(WHITE);
  display.setCursor(10, 0);
  display.println(text);
  display.display();      // Show initial text
}

void setup() {
  Serial.begin(9600);
  randomSeed(analogRead(0));

   // SSD1306_SWITCHCAPVCC = generate display voltage from 3.3V internally
  if(!display.begin(SSD1306_SWITCHCAPVCC, 0x3C)) { // Address 0x3D for 128x64
    Serial.println(F("SSD1306 allocation failed"));
    for(;;); // Don't proceed, loop forever
  }
  display.display();
  display.clearDisplay();
  display.display();

  pinMode(R_BUTTON, INPUT);
  pinMode(W_BUTTON, INPUT);

  pinMode(DO_NOT_CLICK_LED, OUTPUT);
  pinMode(CLICK_LED, OUTPUT);

  pinMode(R_SCORE_0, OUTPUT);
  pinMode(R_SCORE_1, OUTPUT);
  pinMode(R_SCORE_2, OUTPUT);

  pinMode(W_SCORE_0, OUTPUT);
  pinMode(W_SCORE_1, OUTPUT);
  pinMode(W_SCORE_2, OUTPUT);
}

/*
  pinMode(inPin, INPUT);    // sets the digital pin 7 as input
*/
void loop() {
restart_game:
  display.clearDisplay();
  display.display();
  pred_score = pwhite_score = 0;
  i = j = k = 0;
  digitalWrite(DO_NOT_CLICK_LED, LOW);
  digitalWrite(CLICK_LED, LOW);
  digitalWrite(R_SCORE_0, LOW);
  digitalWrite(R_SCORE_1, LOW);
  digitalWrite(R_SCORE_2, LOW);
  digitalWrite(W_SCORE_0, LOW);
  digitalWrite(W_SCORE_1, LOW);
  digitalWrite(W_SCORE_2, LOW);

restart:
  delay(3000);

  display.clearDisplay();
  display.display();
  showtext("GO!");

  for (i = 0; i < 10 * 5; ++i)
  {
    randNbr0 = random(2);
    randNbr1 = random(2);
    j = randNbr0 == 1 ? HIGH : LOW;
    k = randNbr1 == 1 ? HIGH : LOW;
    digitalWrite(DO_NOT_CLICK_LED, j);
    digitalWrite(CLICK_LED, k);
    delay(100);
  }

  randNbr0 = random(2);

  if (randNbr0 == 0)
  {
    digitalWrite(DO_NOT_CLICK_LED, LOW);
    digitalWrite(CLICK_LED, HIGH);
  }
  else
  {
    digitalWrite(DO_NOT_CLICK_LED, HIGH);
    digitalWrite(CLICK_LED, LOW);
  }
  i =  j = LOW;
  k = 0;
  while (k < 3 * 100)
  {
    ++k;
    i = digitalRead(R_BUTTON);
    j = digitalRead(W_BUTTON);
    
    if (i == HIGH && j == HIGH)
    {
      Serial.println("It was a draw");
      goto restart;
    }
    else if (i == HIGH)
    {
      Serial.println("Player Red");
      break;
    }
    else if (j == HIGH)
    {
      Serial.println("Player White");
      break;
    }
    delay(10);
    i =  j = LOW;
  }

  if (i == HIGH)
  {
    if (randNbr0 == 0) {
      ++pred_score;
      showtext("RED SCORES!");
      display.display();
    } else if (pred_score > 0) {
      --pred_score;
      showtext("RED MINUS!");
      display.display();
    }
  }
  else
  {
    if (randNbr0 == 0) {
      ++pwhite_score;
      showtext("WHITE SCORES!");
      display.display();
    } else if (pwhite_score > 0) {
      --pwhite_score;
      showtext("WHITE MINUS!");
      display.display();
    }
  }

  for (i = 0; i < pred_score; ++i)
  {
    digitalWrite(R_SCORE_0 + i, HIGH);
  }
  for (; i < 3; ++i)
  {
    digitalWrite(R_SCORE_0 + i, LOW);
  }

  for (j = 0; j < pwhite_score; ++j)
  {
    digitalWrite(W_SCORE_0 + j, HIGH);
  }
  for (; j < 3; ++j)
  {
    digitalWrite(W_SCORE_0 + j, LOW);
  }
  if (pred_score == 3 || pwhite_score == 3)
  {
    if (pred_score == 3) {
      Serial.println("RED WINS");
      showtext("WHITE PLAYER DRINK!");
      display.display();
    } else {
      Serial.println("WHITE WINS");
      showtext("RED PLAYER DRINK!");
      display.display();
    }
    delay(5000);
    goto restart_game;
  }
  goto restart;
}
